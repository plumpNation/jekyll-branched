# Jekyll - Branched

All you can find here is this README file in the `master` branch. GitLab Pages
live in the `pages` branch.

This way you can have your project's code in the `master` branch and use an
orphan branch (`pages`) for hosting your site. Learn more about it in the
[documentation].

View the Pages source content: https://gitlab.com/plumpNation/jekyll-branched/tree/pages

View site: https://plumpnation.gitlab.io/jekyll-branched

[GitLab Pages]: https://pages.gitlab.io
[documentation]: http://doc.gitlab.com/ee/pages/README.html#how-to-set-up-gitlab-pages-in-a-repository-where-theres-also-actual-code
